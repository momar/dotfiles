[[ -o login ]] && source /etc/profile
source /opt/antigen.zsh

antigen use oh-my-zsh

antigen bundles <<EOBUNDLES

extract  # Helper for extracting different types of archives.
docker
colored-man-pages
sudo  # Double-Escape to toggle sudo
tarruda/zsh-autosuggestions

mafredri/zsh-async
sindresorhus/pure

EOBUNDLES
antigen apply

unsetopt correct
setopt no_share_history

# Never cache command completions
zstyle ":completion:*:commands" rehash 1

if which exa >/dev/null; then
	alias ls="exa -I snap --git-ignore"
	alias la="exa -a"
	alias l="exa -I snap --git-ignore --git -l"
	alias ll="exa --git -la"
else
	alias ls="ls --color=always"
	alias la="ls -A"
	alias l="ls -l"
	alias ll="ls -lA"
fi
alias lsl="ls"
alias sls="ls"
alias lsls="ls"
alias sl="ls"

alias qbin="curl https://qbin.io -s -T"
transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi; tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; }
alias pw='bash -c '"'"'echo `tr -dc $([ $# -gt 1 ] && echo $2 || echo "A-Za-z0-9") < /dev/urandom | head -c $([ $# -gt 0 ] && echo $1 || echo 30)`'"'"' --'
alias yq="python3 -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)' | jq"

alias d="docker"
alias dc="docker-compose"

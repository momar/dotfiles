export EDITOR=/usr/bin/nano
export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

export MOZ_USE_XINPUT2=1

export NPM_PACKAGES="${HOME}/.local/share/node_modules"
unset MANPATH; export MANPATH="$NPM_PACKAGES/share/man:$(manpath)"

export GOROOT="/usr/lib/go-1.12"
export GOPATH="${HOME}/.local/share/gopath"

export PATH="$GOPATH/bin:$NPM_PACKAGES/bin:$HOME/.local/bin:/usr/lib/go-1.12/bin:$PATH"

home:
	find home -mindepth 1 -maxdepth 1 -exec echo rsync -a {} ~/ \;
	@read -p "Git author name: " value && sed "s@GIT_AUTHOR_NAME@$value@g" -i ~/.gitconfig
	@read -p "Git author email: " value && sed "s@GIT_AUTHOR_MAIL@$value@g" -i ~/.gitconfig
	@read -p "surge.sh login: " value && sed "s@SURGE_USERNAME@$value@g" -i ~/.netrc
	@read -s -p "surge.sh password: " value && sed "s@SURGE_PASSWORD@$value@g" -i ~/.netrc
	npm login
	sudo docker login
	@echo TODO: log into Firefox, Thunderbird, GitKraken & Zoom
	# TODO: firefox & thunderbird ootb-config

install:
	# Docker
	which docker >/dev/null || { curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"; }
	# Peek
	which peek >/dev/null || { sudo add-apt-repository ppa:peek-developers/stable; }
	# node.js
	which node >/dev/null || { curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -; }
	# System packaes
	sudo apt-get -y install \
		vim nano tmux zsh sudo git build-essential zip unzip tar htop wget curl net-tools nethogs gnupg tree software-properties-common apt-transport-https mosh silversearcher-ag jq mycli pgcli python3-pip python-setuptools sqlite3 bzip2 dnsutils nmap lshw ncdu \
		fd-find exa gron entr aria2 \
		ansible \
		docker-ce docker-ce-cli containerd.io docker-compose \
		nodejs cryfs zsh-antigen golang-1.12-go openjdk-8-jdk peek
	sudo git clone https://github.com/moqmar/prompt.git /opt/prompt
	# missing tools from ansible role (TODO: upgrade)
	which hexyl >/dev/null || { wget https://github.com/sharkdp/hexyl/releases/download/v0.3.1/hexyl_0.3.1_amd64.deb -O /tmp/hexyl.deb && sudo dpkg -i /tmp/hexyl.deb; }
	which bat >/dev/null || { wget https://github.com/sharkdp/bat/releases/download/v0.9.0/bat_0.9.0_amd64.deb -O /tmp/bat.deb && sudo dpkg -i /tmp/bat.deb; }
	which kak >/dev/null || { git clone http://github.com/mawww/kakoune.git /tmp/kakoune && cd /tmp/kakoune/src && make && sudo cp kak /usr/local/bin/kak; }
	which ctop >/dev/null || { wget https://github.com/bcicen/ctop/releases/download/v0.7.2/ctop-0.7.2-linux-amd64 -O /usr/local/bin/ctop && sudo chmod +x /usr/local/bin/ctop; }
	# other tools from other package managers
	curl https://getcaddy.com | bash -s personal
	sudo pip3 install --upgrade litecli
	go get -u github.com/go-bindata/go-bindata/... github.com/zyedidia/micro/cmd/micro
	npm i -g surge elm stylus sass

graphical-install: install
	which zoom >/dev/null || { wget https://zoom.us/client/latest/zoom_amd64.deb -O /tmp/zoom.deb && sudo dpkg -i /tmp/zoom.deb; }
	which gitkraken >/dev/null || { wget https://www.gitkraken.com/download/linux-deb -O /tmp/gitkraken.deb && sudo dpkg -i /tmp/gitkraken.deb; }
	which steam >/dev/null || { wget https://steamcdn-a.akamaihd.net/client/installer/steam.deb -O /tmp/steam.deb && sudo dpkg -i /tmp/steam.deb; }
	sudo apt-get -y install \
		libreoffice \
		blender gimp gthumb simple-scan inkscape audacity olive-editor pavucontrol mpv \
		vscodium tilix filezilla mumble \
		firefox thunderbird \
		remmina remmina-plugin-rdp remmina-plugin-vnc \
		qalculate-gtk yubioath-desktop \
		wine-development virtualbox woeusb \
		nextcloud-desktop
	[ -f ~/.local/opt/Etcher.AppImage ] || { wget https://github.com/balena-io/etcher/releases/download/v1.5.50/balena-etcher-electron-1.5.50-linux-x64.zip -O /tmp/balena.zip && unzip /tmp/balena.zip && mv "balenaEtcher-*-x64.AppImage" ~/.local/opt/Etcher.AppImage; }
	# themes & icons
	[ -d ~/.themes/Adementary ] || { git clone https://github.com/hrdwrrsk/adementary-theme.git /tmp/adementary && cd /tmp/adementary && ./install.sh -d ~/.themes; }
	[ -d /usr/share/icons/Papirus ] || { sudo add-apt-repository ppa:papirus/papirus && sudo apt-get -y install papirus-icon-theme; }
	# TODO: check name
	[ -d /usr/share/cursors/Capitaine ] || { sudo add-apt-repository ppa:dyatlov-igor/la-capitaine && sudo apt-get -y install la-capitaine-cursor-theme }
	@echo TODO: install Aseprite from Humble Bundle
